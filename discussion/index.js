//CRUD Operations


//Create
	// - to insert a document/s

	//insertOne method
	db.collections.insertOne({document});

	db.users.insertOne(
		{
			"firstName": "Jane",
			"lastName": "Doe",
			"age": 21,
			"contact": {
				"phone": "87654321",
				"email": "janedoe@gmail.com"
			},
			"courses": ["CSS", "Javascript", "Python"],
			"department": "none"
		}
	);

	//insertMany method
	db.collections.insertMany([{doc1}, {{doc2}, ...}]);

	db.users.insertMany(
		[
			{
				"firstName": "Stephen",
				"lastName": "Hawking",
				"age": 76,
				"contact": {
					"phone": "87654321",
					"email": "stephenhawking@gmail.com"
				},
				"courses": ["Python", "React", "PHP"],
				"department": "none"
			}, 
			{
				"firstName": "Neil",
				"lastName": "Armstrong",
				"age": 82,
				"contact": {
					"phone": "87654321",
					"email": "neilarmstrong@gmail.com"
				},
				"courses": ["React", "Laravel", "Sass"],
				"department": "none"
			}, 
		]
	);

//Read Operation
	// - retreive documents from the collection

	db.collections.find({query}, {field projection})

	//find() method
	db.users.find();


//Update Operation
	// - update a document/s

	db.collections.updateOne({filter}, {update})
	db.collections.updateMany()


	//add first a document to be modified
	db.users.insertOne(
		{
			{
				"firstName": "Test",
				"lastName": "Test",
				"age": 0,
				"contact": {
					"phone": "0",
					"email": "test@gmail.com"
				},
				"courses": [],
				"department": "none"
			}
		}
	);

	//modifying the newly added document using updateOne() method
	db.users.updateOne(
		{filter},
		{update}
	);

	//use first name field as a filter and look for the name test
	//using update operator set, update the fields of the matching document with the following details
		// bill gates, 65yo, phone: 12345678, bill@gmail.com, courses PHP, Laravel, HTML, operations dept, status: active
	db.users.updateOne(
		{"firstName": "Test"},
		{
			$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"contact": {
					"phone": "12345678",
					"email": "bill@gmail.com"
				},
				"courses": ["PHP", "Laravel", "HTML"],
				"department": "Operations",
				"status": "active"
			}
		}
	);


	//updateMany()
		//use department field as a filter and look for value none
		//using update operator, update the department field from value none to hr 
		//run find method to check if department fields of the matching document is updated
		
	db.users.updateMany(
		{"department": "none"},
		{
			$set: {
				"department": "HR"
			}
		}
	);

	//Mini Activity
		// look for a document that has a field status, using object id as filter
		//remove the status field using an update operator
		db.users.updateOne(
			{"_id" : ObjectId("61e7fd7a1a4e7116cca6b94a")},
			{
				$unset: {
					"status": "active"
				}
			}
		);


//Delete Operation
	// - delete a document/s

	db.collections.deleteOne({filter})
	db.collections.deleteMany({filter})

	//insert a document as an example to be deleted
	db.users.insertOne({"firstName": "Joy", "lastName": "Pague"})

	//deleteOne() method
	db.users.deleteOne(
		{"firstName": "Joy"}
	);



	//insert a document as an example to be deleted
	db.users.insertOne({"firstName": "Bill", "lastName": "Crawford"})

	//deleteMany()
	db.users.deleteMany({"firstName": "Bill"})